<?php

namespace App\Http\Controllers\Teknisi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tiket;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\RiwayatTiket;

class AduanSelesaiController extends Controller
{
    public function index()
    {
        $teknisiId = Auth::id();

        $tikets = Tiket::where('id_pengguna', $teknisiId)->where('status_tiket', 'Selesai')->with('lampiran')->get()->map(function ($tiket) {
            foreach ($tiket->getAttributes() as $key => $value) {
                if ($value === null) {
                    $tiket->{$key} = '---';
                }
            }

            switch ($tiket->status_tiket) {
                case 'Selesai':
                    $tiket->status_tiket = '<span class="badge bg-success">Selesai</span>';
                    break;
                case 'Sedang Diproses':
                    $tiket->status_tiket = '<span class="badge bg-warning">Sedang Diproses</span>';
                    break;
                case 'Belum Diproses':
                    $tiket->status_tiket = '<span class="badge bg-danger">Belum Diproses</span>';
                    break;
                default:
                    $tiket->status_tiket = '---';
            }

            $tiket->file_tiket = $tiket->lampiran->first() && $tiket->lampiran->first()->file_tiket
                ? '<button type="button" class="btn btn-secondary btn-sm btn-icon-text" onclick="openModal(\'/storage/' . $tiket->lampiran->first()->file_tiket . '\')">Lihat File</button>'
                : '---';

            $tiket->tanggal_masuk = date_format(date_create($tiket->tanggal_masuk), 'd/m/Y');
            return $tiket;
        });
        foreach ($tikets as $tiket) {
            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }
        }

        if (!$tikets->isEmpty()) {
            $riwayatTiket = RiwayatTiket::where('deskripsi_riwayat', 'Laporan sudah selesai')->first();
            $riwayatTiket->waktu_riwayat = Carbon::parse($riwayatTiket->waktu_riwayat)->format('d/m/Y');

            return view(
                '/teknisi/aduan_selesai',
                [
                    'tikets' => $tikets,
                    'riwayatTiket' => $riwayatTiket,
                ]
            );
        }

        return view(
            '/teknisi/aduan_selesai',
            [
                'tikets' => $tikets,
            ]
        );
    }
}
