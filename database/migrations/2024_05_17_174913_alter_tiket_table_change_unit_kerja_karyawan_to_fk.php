<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tiket', function (Blueprint $table) {
            // Drop the existing column
            $table->dropColumn('unit_kerja_karyawan');

            // Add new foreign key column
            $table->unsignedBigInteger('unit_kerja_id')->nullable()->after('nik_karyawan');

            // Add foreign key constraint
            $table->foreign('unit_kerja_id')->references('id')->on('unit_kerja')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tiket', function (Blueprint $table) {
            // Drop foreign key and column
            $table->dropForeign(['unit_kerja_id']);
            $table->dropColumn('unit_kerja_id');

            // Add the original column back
            $table->enum('unit_kerja_karyawan', ['TU', 'OB', 'BEM'])->nullable()->after('nik_karyawan');
        });
    }
};
