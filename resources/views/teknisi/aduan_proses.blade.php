@extends('teknisi.template.main')

@section('title', 'Data Aduan Sedang Diproses - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Tiket Pengaduan</h6>
                            <div class="d-flex align-items-center flex-wrap text-nowrap" id="bt-group">
                                <div class="input-group date datepicker wd-200 me-2 mb-2 mb-md-0" id="dashboardDate">
                                    <span class="input-group-text input-group-addon bg-transparent border-success"><i
                                            data-feather="calendar" class=" text-success"></i></span>
                                    <input type="text" class="form-control border-success bg-transparent" id="bt-date">
                                </div>
                                <button type="button" class="btn btn-success btn-icon-text mb-2 mb-md-0 text-light"
                                    id="bt-download">
                                    <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                                    Download Laporan
                                </button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="tabelAduanProses" class="table hover stripe" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Posisi</th>
                                        <th>Telepon</th>
                                        <th>Kategori Masalah</th>
                                        <th>Deskripsi Masalah</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Estimasi Selesai</th>
                                        <th>File Tiket</th>
                                        <th>Tanggapan</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    @php $i = 1 @endphp
                                    @forelse ($tikets as $tiket)
                                        <input type="hidden" name="id" value="{{ $tiket->id }}">
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $tiket->nama }}</td>
                                            <td>{{ $tiket->posisi }}</td>
                                            <td><a href="https://wa.me/{{ $tiket->no_telepon }}">{{ $tiket->no_telepon }}
                                            </td>
                                            <td>{{ $tiket->kategori_laporan == 'Lainnya' ? $tiket->kategori_lainnya : $tiket->kategori_laporan }}
                                            </td>
                                            <td>{{ $tiket->deskripsi_tiket }}</td>
                                            <td>{{ $tiket->tanggal_pengerjaan }}</td>
                                            <td>{{ $tiket->estimasi_selesai }}</td>
                                            <td><button class="btn btn-success btn-sm" id="teknisi_selesai"
                                                    data-tiket-id="{{ $tiket->id }}">Selesai</button></td>
                                        </tr>
                                    @empty
                                        <td colspan="9" class="text-center">Belum ada tiket yang sedang ditangani!
                                        </td>
                                    @endforelse
                                </tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImageLabel">File Image</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img id="modalImageContent" src="" alt="File Image" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        function openModal(imageSrc) {
            document.getElementById('modalImageContent').src = imageSrc;
            var modal = new bootstrap.Modal(document.getElementById('modalImage'));
            modal.show();
        }

        document.getElementById('bt-download').addEventListener('click', function() {
            var selectedDate = document.getElementById('bt-date').value;
            window.location.href = '/generate-excel-proses?date=' + encodeURIComponent(selectedDate);
        });

        var tabel;
        // read data pengguna
        $(document).ready(function() {
            tabel = $('#tabelAduanProses').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('tksaduanproses') }}",
                columns: [{
                        data: null,
                        name: 'id',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'posisi',
                        name: 'posisi'
                    },
                    {
                        "data": "no_telepon",
                        render: function(data, type, row, meta) {
                            return '<a href="https://wa.me/' + data + '" target="_blank">' + data +
                                '</a>';
                        },
                        name: 'no_telepon'
                    },
                    {
                        data: 'kategori',
                        name: 'kategori'
                    },
                    {
                        data: 'deskripsi_tiket',
                        name: 'deskripsi_tiket'
                    },
                    {
                        data: 'tanggal_pengerjaan',
                        name: 'tanggal_pengerjaan'
                    },
                    {
                        data: 'estimasi_selesai',
                        name: 'estimasi_selesai'
                    },
                    {
                        "data": "file_tiket",
                        render: function(data, type, row, meta) {
                            if (data !== '---') {
                                return '<button type="button" class="btn btn-secondary btn-sm btn-icon-text" onclick="openModal(\'/storage/' +
                                    data + '\')">Lihat File</button>';
                            } else {
                                return '---';
                            }
                        }
                    },
                    {
                        data: null,
                        name: 'aksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return `<button class="btn btn-success btn-sm" id="teknisi_selesai" data-tiket-id="` +
                                row.id + `">Selesai</button>`;
                        }
                    }
                ],
                aLengthMenu: [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                iDisplayLength: 10,
                language: {
                    search: "",
                    paginate: {
                        previous: "Sebelumnya",
                        next: "Selanjutnya"
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    search: "Cari:",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    zeroRecords: "Belum ada tiket yang sedang ditangani!",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                    infoFiltered: "(disaring dari _MAX_ entri keseluruhan)"
                },
                responsive: true,
                "columnDefs": [{
                    "orderable": false,
                    "targets": 0
                }]
            });

            $('#tabelAduanProses').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });
        });

        $(document).on('click', '#teknisi_selesai', function(e) {
            e.preventDefault();
            var tiketId = $(this).data('tiket-id');

            Swal.fire({
                title: "Apakah proyek ini sudah selesai?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Selesai"
            }).then((result) => {
                if (result.isConfirmed) {
                    //update
                    $.ajax({
                        type: 'POST',
                        url: '/teknisi/aduan-proses/update',
                        data: {
                            _token: '{{ csrf_token() }}',
                            tiketId: tiketId,
                        },
                        success: function(response) {
                            Swal.fire({
                                title: "Berhasil!",
                                text: "Tiket telah selesai.",
                                icon: "success"
                            });
                            tabel.ajax.reload();
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Gagal! ✋😌',
                                text: 'Coba lagi!',
                                confirmButtonText: 'OK'
                            });
                        }
                    });

                    // //store
                    // $.ajax({
                    //     type: 'POST',
                    //     url: '/teknisi/aduan-proses/store',
                    //     data: {
                    //         _token: '{{ csrf_token() }}',
                    //         tiketId: tiketId,
                    //     },
                    //     success: function(response) {
                    //         Swal.fire({
                    //             title: "Berhasil!",
                    //             text: "Tiket telah selesai.",
                    //             icon: "success"
                    //         }).then((result) => {
                    //             if (result.isConfirmed) {

                    //             }
                    //         });
                    //     },
                    //     error: function(xhr, status, error) {
                    //         Swal.fire({
                    //             icon: 'error',
                    //             title: 'Gagal! ✋😌',
                    //             text: 'Coba lagi!',
                    //             confirmButtonText: 'OK'
                    //         });
                    //     }
                    // });
                }
            });
        });

        $(window).resize(function() {
            $('#tabelAduanProses').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush


@push('style')
    <style>
        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        #tabelAduanProses td,
        #tabelAduanProses th {
            text-align: center;
        }

        #tabelAduanProses td.child {
            text-align: left;
        }

        #tabelAduanProses thead th:first-child {
            cursor: default;
        }

        #tabelAduanProses thead th:first-child::after,
        #tabelAduanProses thead th:first-child::before {
            display: none !important;
            pointer-events: none;
        }

        @media (max-width: 768px) {
            #top-content {
                flex-direction: column;
            }

            #bt-group {
                justify-content: center;
                margin-top: 10px;
                margin-bottom: -20px;
            }

            #bt-download {
                display: block;
                width: 60%;
            }

            #tabelAduanProses td {
                white-space: normal;
                word-wrap: break-word;
            }

            #tabelAduanProses_filter {
                margin-top: 10px;
            }
        }

        @media (max-width: 468px) {
            #bt-download {
                width: 80%;
            }
        }

        @media (max-width: 384px) {
            #bt-download {
                width: 90%;
            }
        }
    </style>
@endpush
