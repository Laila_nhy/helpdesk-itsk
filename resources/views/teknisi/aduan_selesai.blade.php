@extends('teknisi.template.main')

@section('title', 'Data Aduan Selesai Diproses - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Tiket Pengaduan</h6>
                            <div class="d-flex align-items-center flex-wrap text-nowrap" id="bt-group">
                                <div class="input-group date datepicker wd-200 me-2 mb-2 mb-md-0" id="dashboardDate">
                                    <span class="input-group-text input-group-addon bg-transparent border-success"><i
                                            data-feather="calendar" class=" text-success"></i></span>
                                    <input type="text" class="form-control border-success bg-transparent" id="bt-date">
                                </div>
                                <button type="button" class="btn btn-success btn-icon-text mb-2 mb-md-0 text-light"
                                    id="bt-download">
                                    <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                                    Download Report
                                </button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="tabelAduanSelesai" class="table hover stripe" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Posisi</th>
                                        <th>Telepon</th>
                                        <th>Kategori Masalah</th>
                                        <th>Deskripsi Masalah</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Tanggal Selesai</th>
                                        <th>File Tiket</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($tikets as $tiket)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $tiket->nama }}</td>
                                            <td>{{ $tiket->posisi }}</td>
                                            <td><a
                                                    href="https://wa.me/{{ $tiket->no_telepon }}">{{ $tiket->no_telepon }}</a>
                                            </td>
                                            <td>{{ $tiket->kategori_laporan == 'Lainnya' ? $tiket->kategori_lainnya : $tiket->kategori_laporan }}
                                            </td>
                                            <td>{{ $tiket->deskripsi_tiket }}</td>
                                            <td>{{ $tiket->tanggal_masuk }}</td>
                                            <td>{{ $riwayatTiket->waktu_riwayat }}</td>
                                            <td>{!! $tiket->file_tiket !!}</td>
                                        </tr>
                                    @empty
                                        <td colspan="8" class="text-center">Belum ada tiket yang selesai!
                                        </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImageLabel">File Image</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img id="modalImageContent" src="" alt="File Image" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#modalEdit').on('hidden.bs.modal', function() {
                $(this).find('input[type=text], input[type=date]').val('');
                $(this).find('select').prop('selectedIndex', 0);
            });
        });

        $(function() {
            $('#tabelAduanSelesai').DataTable({
                "aLengthMenu": [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                "iDisplayLength": 10,
                "language": {
                    search: "",
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Selanjutnya"
                    },
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ entri",
                    "zeroRecords": "Tidak ditemukan data yang sesuai",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                    "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
                },
                "responsive": true
            });

            $('#tabelAduanSelesai').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });
        });

        $(window).resize(function() {
            $('#tabelAduanSelesai').DataTable().columns.adjust().responsive.recalc();
        });

        function openModal(imageSrc) {
            document.getElementById('modalImageContent').src = imageSrc;
            var modal = new bootstrap.Modal(document.getElementById('modalImage'));
            modal.show();
        }

        document.getElementById('bt-download').addEventListener('click', function() {
            var selectedDate = document.getElementById('bt-date').value;
            window.location.href = '/generate-excel-selesai?date=' + encodeURIComponent(selectedDate);
        });
    </script>
@endpush

@push('style')
    <style>
        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        #tabelAduanSelesai td,
        #tabelAduanSelesai th {
            text-align: center;
        }

        #tabelAduanSelesai td.child {
            text-align: left;
        }

        @media (max-width: 768px) {
            #top-content {
                flex-direction: column;
            }

            #bt-group {
                justify-content: center;
                margin-top: 10px;
                margin-bottom: -20px;
            }

            #bt-download {
                display: block;
                width: 60%;
            }

            #tabelAduanSelesai td {
                white-space: normal;
                word-wrap: break-word;
            }

            #tabelAduanSelesai_filter {
                margin-top: 10px;
            }
        }

        @media (max-width: 468px) {
            #bt-download {
                width: 80%;
            }
        }

        @media (max-width: 384px) {
            #bt-download {
                width: 90%;
            }
        }
    </style>
@endpush
